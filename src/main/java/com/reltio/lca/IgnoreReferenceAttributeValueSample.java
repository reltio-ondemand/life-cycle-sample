package com.reltio.lca;

import com.fasterxml.reltio.jackson.databind.ObjectMapper;
import com.reltio.lifecycle.framework.*;
import java.util.List;
import java.util.logging.Logger;

public class IgnoreReferenceAttributeValueSample extends LifeCycleActionBase {

    private final ObjectMapper mapper = new ObjectMapper();
    private final Logger logger = Logger.getLogger(IgnoreReferenceAttributeValueSample.class.getName());

    public void afterSave(IReltioAPI reltioAPI, ILifeCycleObjectData data) {

        List<IAttributeValue> addressValues = data.getObject().getAttributes().getAttributeValues("Address");
        for (IAttributeValue currentAddressValue : addressValues) {
            for (ICrosswalk currentAddressCrosswalk: ((IReferenceAttributeValue)currentAddressValue).getReferencedRelation().getCrosswalks().getCrosswalks()) {
                if ("configuration/sources/IMS".equals(currentAddressCrosswalk.getType())) {
                    reltioAPI.put(currentAddressValue.getUri() + "/ignore", null, null);
                    continue;
                }
            }
        }
    }

}