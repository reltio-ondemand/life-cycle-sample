package com.reltio.lca;


import com.fasterxml.reltio.jackson.databind.ObjectMapper;
import com.reltio.lifecycle.framework.*;

import java.io.InputStream;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;
import org.apache.log4j.Logger;
import java.util.concurrent.ConcurrentSkipListSet;

public class StartWorkflowSample extends LifeCycleActionBase {
    private final Set<String> urisInProcess = new ConcurrentSkipListSet<>();
    private final ObjectMapper mapper = new ObjectMapper();
    private final Logger logger = Logger.getLogger(StartWorkflowSample.class.getName());
    private String userName = "sampleUserName";
    private String password = "samplePassword";

    @Override
    public void afterSave(IReltioAPI reltioAPI, ILifeCycleObjectData data) {
        IObject object = data.getObject();
        if (object == null) {
            return;
        }
        IAttributes attributes = object.getAttributes();
        if (attributes == null) {
            return;
        }
        ICrosswalks crosswalks = object.getCrosswalks();
        if (crosswalks == null || crosswalks.getCrosswalks().isEmpty()) {
            return;
        }
        String uri = object.getUri();
        if (!urisInProcess.add(uri)) {
            return;
        }
        try {
            Map<String, Object> attributesBody = new HashMap<>();
            String sampleCrosswalkValue = null;
            for (ICrosswalk cw : crosswalks.getCrosswalks()) {
                if (cw.getType().equals("configuration/sources/StartWorkflowSourceSystem")) {
                    sampleCrosswalkValue = cw.getValue();
                }
            }

            List<IAttributeValue> checkboxes = attributes.getAttributeValues("ReadyForReview");
            if (!checkboxes.isEmpty()) {
                ISimpleAttributeValue checkBox = (ISimpleAttributeValue) checkboxes.get(0);
                if (checkBox.getBooleanValue()) {
                    String workflow = null;
                    Map<String, Object> workflowBody = new HashMap<>();
                    workflowBody.put("processType", "readyForReview");
                    workflowBody.put("objectURIs", Collections.singletonList(uri));
                    workflow = mapper.writeValueAsString(workflowBody);
                    String token = getAuthorizationToken();
                    startWorkflow(workflow, token);
                    attributesBody.put("Status", Collections.singletonList(Collections.singletonMap("value", "Pending")));
                    Map<String, Object> overrideBody = new HashMap<>();
                    overrideBody.put("type", object.getType());

                    List<Map<String, Object>> crosswalksBody = new ArrayList<>(2);
                    Map<String, Object> sampleCrosswalkBody = new HashMap<>();
                    sampleCrosswalkBody.put("type", "configuration/sources/StartWorkflowSourceSystem");
                    if (sampleCrosswalkValue == null) {
                        String attributeUri = object.getAttributes().createSimpleAttributeValue("Status").build().getUri();
                        sampleCrosswalkValue = attributeUri.substring(attributeUri.lastIndexOf("/") + 1);
                    }
                    sampleCrosswalkBody.put("value", sampleCrosswalkValue);
                    sampleCrosswalkBody.put("dataProvider", Boolean.TRUE);
                    crosswalksBody.add(sampleCrosswalkBody);
                    overrideBody.put("crosswalks", crosswalksBody);

                    overrideBody.put("attributes", attributesBody);

                    String requestBody = mapper.writeValueAsString(Collections.singletonList(overrideBody));
                    reltioAPI.post("entities", requestBody, null);
                }
            }


        } catch (Exception ex) {
            logger.error(ex);
            throw new RuntimeException("StartWorkflowSample: LCA invokation failed.");
        }
    }


    private String getAuthorizationToken() throws Exception {
        String token = null;
        URL url = new URL("https://auth.reltio.com/v2/oauth/token?grant_type=password&username=" + userName + "&password=" + password);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setDoOutput(true);
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("Authorization", "Basic cmVsdGlvX3VpOm1ha2l0YQ==");
        conn.connect();

        InputStream is = conn.getInputStream();
        Map result = mapper.readValue(is, Map.class);
        is.close();
        token = (String) result.get("access_token");
        return token;
    }

    private void startWorkflow(String workflow, String token) throws Exception {
        if (workflow != null) {
            URL url = new URL("https://workflow-pilot.reltio.com/workflow-adapter/workflow/xG4m7jOymGLejr6/processInstances");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setRequestProperty("Source-System", "StartWorkflowSourceSystem");
            conn.setRequestProperty("EnvironmentURL", "https://pilot.reltio.com/");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Authorization", "Bearer " + token);
            conn.connect();
            PrintStream httpOut = new PrintStream(conn.getOutputStream(), false, StandardCharsets.UTF_8.name());
            httpOut.print(workflow);
            httpOut.flush();
            httpOut.close();
            InputStream is = conn.getInputStream();
            mapper.readValue(is, Map.class);
            is.close();
        }
    }
}