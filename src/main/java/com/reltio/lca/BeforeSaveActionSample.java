package com.reltio.lca;

import com.reltio.lifecycle.framework.*;
import org.apache.log4j.Logger;

import java.util.List;


public class BeforeSaveActionSample extends LifeCycleActionBase {

    private final Logger logger = Logger.getLogger(BeforeSaveActionSample.class.getName());

    @Override
    public ILifeCycleObjectData beforeSave(IReltioAPI reltioAPI, ILifeCycleObjectData data) {
        IObject object = data.getObject();
        if (object == null) {
            return null;
        }
        IAttributes attributes = object.getAttributes();
        if (attributes == null) {
            return null;
        }
        ICrosswalks crosswalks = object.getCrosswalks();
        if (crosswalks == null || crosswalks.getCrosswalks().isEmpty()) {
            return null;
        }

        try {
            boolean updated = false;

            ICrosswalk twitterCrosswalk = null;
            for (ICrosswalk cw : crosswalks.getCrosswalks()) {
                if (!cw.getType().equals("configuration/sources/TWITTER")) {
                    twitterCrosswalk = cw;
                    break;
                }
            }

            if (twitterCrosswalk == null) {
                return null;
            }
            List<IAttributeValue> fullNames = attributes.getAttributeValues("Name");
            if (fullNames.isEmpty()) {
                List<IAttributeValue> firstNames = attributes.getAttributeValues("FirstName");
                List<IAttributeValue> lastNames = attributes.getAttributeValues("LastName");
                if (!firstNames.isEmpty() && !lastNames.isEmpty()) {
                    updated = true;
                    ISimpleAttributeValue firstName = (ISimpleAttributeValue) firstNames.get(0);
                    ISimpleAttributeValue lastName = (ISimpleAttributeValue) lastNames.get(0);
                    String first = firstName.getStringValue();
                    String last = lastName.getStringValue();
                    String concatName = first + " " + last;

                    ISimpleAttributeValue newValue = attributes.createSimpleAttributeValue("Name").value(concatName).build();
                    attributes.addAttributeValue(newValue);
                    twitterCrosswalk.getAttributes().addAttributeURI(newValue.getUri());
                }
            }

            if (updated) {
                return data;
            } else {
                return null;
            }
        } catch (Exception ex) {
            logger.error(String.format("beforesave failed for tenant = %s , uri = %s", data.getTenant(), data.getObject().getUri()));
            throw new RuntimeException("BeforeSaveActionSample: LCA invocation failed.", ex);
        }
    }
}
