package com.reltio.lca;

import com.reltio.lifecycle.framework.*;

import java.util.List;
import java.util.UUID;

public class AttributeValueGeneratingSample extends LifeCycleActionBase {

    @Override
    public ILifeCycleObjectData beforeSave(IReltioAPI reltioAPI, ILifeCycleObjectData data) {

        IObject myObject = data.getObject();
        String uri = myObject.getUri();

        IAttributes incomingAttributes = myObject.getAttributes();

        //Generating the Random specific ID
        String specificID = null;

        List<IAttributeValue> specificIDAttributeValues = incomingAttributes.getAttributeValues("SpecificID");

        if (specificIDAttributeValues != null && !specificIDAttributeValues.isEmpty()) {
            for (IAttributeValue currentValue : specificIDAttributeValues) {
                if (currentValue instanceof ISimpleAttributeValue) {
                    ISimpleAttributeValue simpleDescription = (ISimpleAttributeValue) currentValue;
                    String str = simpleDescription.getStringValue();
                    if (str == null || str.trim().length() == 0) {
                        specificID = UUID.randomUUID().toString();
                        simpleDescription.setValue(specificID);
                    }

                }
            }
        } else {
            specificID = UUID.randomUUID().toString();
            IAttributeValue value = incomingAttributes.createSimpleAttributeValue("SpecificID").value(specificID).build();
            incomingAttributes.addAttributeValue(value);
            myObject.getCrosswalks().getCrosswalks().get(0).getAttributes().addAttribute(value);
        }
        return data;
    }
}