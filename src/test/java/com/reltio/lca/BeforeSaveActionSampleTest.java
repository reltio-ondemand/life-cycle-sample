package com.reltio.lca;

import com.fasterxml.reltio.jackson.databind.JsonNode;
import com.fasterxml.reltio.jackson.databind.ObjectMapper;
import com.reltio.lifecycle.test.LifecycleExecutor;
import org.junit.Test;
import com.reltio.microservices.lcaservice.service.services.LifeCycleHook;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;

public class BeforeSaveActionSampleTest {
    @Test
    public void test() throws Exception {
        BeforeSaveActionSample handler = new BeforeSaveActionSample();
        LifecycleExecutor executor = new LifecycleExecutor();
        String input = new String(Files.readAllBytes(Paths.get(getClass().getResource("/input.json").getPath())));

        String result = executor.executeAction(handler, LifeCycleHook.beforeSave, input);

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readValue(result, JsonNode.class);
        String name = jsonNode.get("object").get("attributes").get("Name").elements().next().get("value").asText();
        assertEquals("John Snow", name);
    }
}
