```
#!txt

Copyright 2016 Reltio 100 Marine Parkway, Suite 275, Redwood Shores, CA USA 94065 (855) 360-DATA www.reltio.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

```
  
[TOC]




# Welcome

Welcome to the project - Reltio LCA Samples!

## Purposes

We have prepared for you the samples of the Reltio LCA implementations in order to help in the start point of the LCA service usage.

## Overview

The Life Cycle Actions Framework (LCAF) provides an easy way to build and deploy custom logic for creating, updating, deleting and performing other operations such as controlling passed data or the prevention of operations. You can apply custom LCAF logic to entities, relationships, interactions, and graphs.

## Service architecture

The Life Cycle Actions (LCA) Service is a standalone service accessible through a LCA Service REST API. You deploy the LCA Service by implementing a Life Cycle Action Handler and placing it into Amazon S3 storage, and configure and monitor the LCA usig the Life Cycle Service REST API.

## LCA Service flow

1. Before LCA invocation, the LCA Service API serializes an object into JSON and passes it to the LCA Service.
2. An LCA handler returns the modified entity serialized into JSON as a result. The handler may also return special results for cases when an object shouldn’t be modified or when the whole operation should be canceled.
3. The LCA Service API applies the modified JSON to the object by substituting the object with result from LCA.
4. LCAs can be injected before operation (synchronous operation) or after operation (asynchronous operation). If a synchronous operation doesn't respond within a specified timeout the LCA Service API cancels the operation returns and logs a corresponding error message.

# LCA code samples #

## "Before Save" Action Sample - class *BeforeSaveActionSample* ##

Suppose we have the following requirement:
If the Individual profile contains a TWITTER crosswalk, then we need to have the "Name" attribute filled. If the "Name" attribute is empty, we construct it by concatenating arbitrary "FirstName" and "LastName" attribute values. LCA works such way that when the hook is called, it gets profile in its input parameter "data". Modifications made to initial object are transferred to API by the returned result of this method. As for the beforeSave action in particular, API saves into database the returned result of this method "as is" if it is not null.

## "After Save" Action Sample - class *IgnoreReferenceAttributeValueSample* ##

Suppose we have the following requirement:
When HCP is created or updated, all values of its reference attribute "Address" should be marked as ignored if the value contains crosswalks from IMS source system. It could be useful as an addition to the survivorship strategy for this attribute. It means that the value containing "IMS" crosswalks will never become an operational value ("ov": true).
In order to implement LCA with such behavior, we define the afterSave method with two loops: external loop and internal loop. In the external loop we iterate through values of the "Address" attribute. Then, inside the internal loop we iterate through particular value crosswalks. If a crosswalk belongs to "IMS" source system, we call Reltio API, in particular http PUT endpoint "ignore attribute value" (reltioAPI.put(currentAddressValue.getUri() + "/ignore", null, null);) and move to the next attribute value.

## Start Workflow Sample - class *StartWorkflowSample* ##

Suppose the purpose of the LCA is to start the "readyForReview" Workflow if the boolean attribute "ReadyForReview" is set to True. The LCA will also set the string attribute "Status" to Pending in this condition.
This is implementing the afterSave hook and needs to be fired when the entity of some type is updated or created.

## Generate New Attribute Value Sample - class *AttributeValueGeneratingSample* ##

Suppose we have the following requirement:
Entities of some type should have at least one "SpecificID" attribute value filled. And such entities do not allow empty (or containing spaces only) values of this attribute.

# Additional information

For more information, please visit 

## [Wiki Home page](https://bitbucket.org/reltio-ondemand/life-cycle-sample/wiki/Home)
## [Life Cycle Hooks](https://bitbucket.org/reltio-ondemand/life-cycle-sample/wiki/Life%20Cycle%20Hooks)
## [How to create and configure Life Cycle Actions](https://bitbucket.org/reltio-ondemand/life-cycle-sample/wiki/How%20to%20create%20and%20configure%20Life%20Cycle%20Actions)
## [Reltio Help Desk](https://help.reltio.com/reltio-manual/features/life-cycle-actions-framework)